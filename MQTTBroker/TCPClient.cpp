#include "TCPClient.h"

TCPClient::TCPClient(const char* address, const char* port):
	m_address(address),
	m_port(port)
{
	if (!reconnect()) 
		closesocket(ConnectSocket);
}

TCPClient::~TCPClient()
{
	if(ConnectSocket != INVALID_SOCKET)
		if (shutdown(ConnectSocket, SD_SEND) == SOCKET_ERROR)
			printf("Client  shutdown failed: %d\n", WSAGetLastError());
	closesocket(ConnectSocket);
	m_treadResive.join();
}

int TCPClient::write(std::string m)
{
	if (send(ConnectSocket, m.c_str(), m.size(), 0) == SOCKET_ERROR) 
	{
		printf("Client send failed: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		return 1;
	}
	return 0;
}

void TCPClient::disconnect()
{
	if (ConnectSocket != INVALID_SOCKET)
		if (shutdown(ConnectSocket, SD_SEND) == SOCKET_ERROR) 
			printf("Client  shutdown failed: %d\n", WSAGetLastError());
	closesocket(ConnectSocket);
}

bool TCPClient::reconnect()
{
	struct addrinfo* result = NULL,
		* ptr = NULL,
		hints;
	int iResult;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	iResult = getaddrinfo(m_address, m_port, &hints, &result);
	if (iResult != 0) 
	{
		printf("Client getaddrinfo failed: %d\n", iResult);
		return false;
	}

	ptr = result;
	ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
		ptr->ai_protocol);

	if (ConnectSocket == INVALID_SOCKET) 
	{
		printf("Client Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		return false;
	}

	iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
	if (iResult == SOCKET_ERROR) 
	{
		closesocket(ConnectSocket);
		ConnectSocket = INVALID_SOCKET;
		return false;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) 
	{
		printf("Client Unable to connect to server!\n");
		return false;
	}

	m_treadResive = std::thread(&TCPClient::resive, this);
	return true;
}


void TCPClient::resive()
{
	int recvbuflen = 512;
	char recvbuf[512];
	int iResult = 0;
	do 
	{
		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) 
		{
			printf("Client Bytes received: %s\n", std::string(recvbuf, iResult).c_str());
		}
		else if (iResult == 0) 
		{
			printf("Client Connection closed\n");
		}
		else 
		{
			int lastError = WSAGetLastError();
			if (lastError != 10004)
				printf("Client recv failed: %d\n", lastError);
		}
	} while (iResult > 0);
	closesocket(ConnectSocket);
}
