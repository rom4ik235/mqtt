#include "MQTTServer.h"
#include <vector>

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c\n"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

namespace MQTTBroker {
	bool initLib() {
		WSADATA wsaData;
		int checkResult;
		// WSAStartup ���������� ��� ������������� ������������� WS2_32.dll
		// �������� MAKEWORD(2, 2) WSAStartup ����������� ������ 2.2 Winsock � �������. 
		// � ������������� ���������� ������
		checkResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (checkResult != 0) {
			printf("Server failed: %d\n", checkResult);
			return false;
		}
		return true;
	}

	void deinitLib()
	{
		WSACleanup();
	}

	MQTTServer::MQTTServer(const char* address, const char* port) 
		:TCPServer(address, port)
	{}

	void MQTTServer::handleMessages(SOCKET& ClientSocket, const char* data, int size)
	{
		//std::unique_ptr<Package> package = wait[ClientSocket];// += std::string(data,size);
		Package& P = wait[ClientSocket];
		if (P.status == PackageStatus::COMPLETED|| P.status == PackageStatus::NOTVALID) {
			P.size = 0;
			P.data.clear();
			P.type = MessType::UNKNOWN;
			P.status = PackageStatus::EMPTY;
			printf("---------------------------------------------------------------------------------------------\n");
		}
		//printf("-------size mess [%d]---------\n", size);
		for (int i = 0; i < size;i++){
			if (P.status == PackageStatus::EMPTY) {
				//printf("TypeMess %d\n", MessType(data[i] >> 4));
				P.type = MessType(data[i] >> 4);
				P.status = PackageStatus::TYPEREAD;
			}
			else if (P.status == PackageStatus::TYPEREAD)
			{
				//printf("Size: %d\n", MessType(data[i]));
				P.size += (data[i]);
				P.status = PackageStatus::LENREAD;
			}
			else if (P.status == PackageStatus::LENREAD) {
				P.data += std::string(data, size);
				//printf("%s", std::string(data,size).c_str());
				if (P.data.size() == P.size) {
					P.status = PackageStatus::COMPLETED;
					onReadPackege(ClientSocket, P);
				}
				i = size;
				//
			}
			else {
				P.status = PackageStatus::NOTVALID;
			}
		}
		//else
		//{
		//	printf("Server Bytes received: %s\n", std::string(data, size).c_str());
		//}
		// ������� ����� ������� �����������
		//std::string str = "";
		//str += (MessType::CONNACK << 4);
		//str += "4";
		//str += "Hi!!";
		//write(ClientSocket, str.c_str(), str.size());
	}

	void MQTTServer::onConnectClient(SOCKET& ClientSocket)
	{
		wait[ClientSocket] = Package();
		printf("Connect Client %d\n", (long)ClientSocket);
	}

	void MQTTServer::onDisconnectClient(SOCKET& ClientSocket)
	{
		wait.erase(ClientSocket);
		printf("Connect Disconnect %d\n", (long)ClientSocket);
	}

	void MQTTServer::onReadPackege(SOCKET& ClientSocket, Package& package)
	{
		switch (package.type)
		{
		case MessType::CONNECT:
		{
			char b1 = package.data[0]; //����� MSB
			char b2 = package.data[1]; //����� �������� �������
			if (package.data[2] == 'M' && package.data[3] == 'Q' &&
				package.data[4] == 'T' && package.data[5] == 'T')
			{
				printf("CONNECT\n");
				char b7 = package.data[6]; //���� ������ ���������(������� ������ ���������)
				 /*
				 8-�� ���� ������������ ������ 
					[7]���� ����� ������������
					[6]���� ������
					[5]���������
					[4-3]����� �� QoS 1-2
					[2]����� ����
					[1]������ ������
					[0]�����������������
				*/
				char b8 = package.data[7];

				
				/*  ��� 9-10 Keep Alive
					Keep Alive  ��� ��������� ��������, ���������� � ��������. 
					���������� ��� 16-������. ��� ������������ ��������� ��������, 
					������� ����� ������ ����� ������, � ������� ������ ����������� 
					�������� ������ ������ ����������, � ������, � ������� �� �������� 
					���������� ���������. ������ ����� ��������������� �� ��, ����� 
					�������� ����� ������������� �������� ���������� �� �������� 
					�������� Keep Alive. ��� ���������� �������� �����-���� ������ 
					������� ���������� ������ ������ ��������� ����� PINGREQ 
				*/
				char b9 = package.data[8];//Keep Alive MSB()
				char b10 = package.data[9];//Keep Alive LSB()
				// 11-n �������� ��������(������) 
				// ��� ����, ���� ��� ����, ������ ���������� � ��������� �������: 
				// �������������� �������, ����� ����������, ���������� � ���������, ���� �������������, ��������
				//printf("id size %d %d %d\n", package.data[9], package.data[10], package.data[11],package.data[12]);
				size_t pos1 = 11;
				size_t pos2 = package.data[11];

				std::string id = package.data.substr(pos1+1, pos2+1);
				if (b8 & ((1 << 7))) {
					pos1 = pos1 + pos2;
					pos2 = package.data[pos1+2];
					std::string username = package.data.substr(pos1+3, pos2+1);
					if (b8 & ((1 << 6))) {
						pos1 = pos1 + pos2+5;
						pos2 = package.data[pos1 -1];
						std::string password = package.data.substr(pos1,pos2);
						printf("id [%s]\nusername [%s]\npassword [%s]\n", id.c_str(), username.c_str(), password.c_str());
					}
				}
			} 
		}
		case MessType::CONNACK:
			printf("CONNACK\n");
			break;
		case MessType::PUBLISH:
			printf("PUBLISH\n");
			break;
		case MessType::PUBACK:
			printf("PUBACK\n");
			break;
		case MessType::PUBREC:
			printf("PUBREC\n");
			break;
		case MessType::PUBREL:
			printf("PUBREL\n");
			break;
		case MessType::PUBCOMP:
			printf("PUBCOMP\n");
			break;
		case MessType::SUNSCRIBE:
			printf("SUNSCRIBE\n");
			break;
		case MessType::SUBACK:
			printf("SUBACK\n");
			break;
		case MessType::UNSUBSCRIBE:
			printf("UNSUBSCRIBE\n");
			break;
		case MessType::UNSUBACK:
			printf("UNSUBACK\n");
			break;
		case MessType::PINGREQ:
			printf("PINGREQ\n");
			break;
		case MessType::PINGRESP:
			printf("PINGRESP\n");
			break;
		case MessType::DISCONNECT:
			printf("DISCONNECT\n");
			break;
		default:
			printf("UNKNOWN\n");
			break;
		}

	}
}