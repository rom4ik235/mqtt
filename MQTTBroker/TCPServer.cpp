#include "TCPServer.h"

void TCPServer::handler(std::shared_ptr<SOCKET> ClientSocket) 
{
	int iResult = 0;
	int recvbuflen = 512;
	do {
		char recvbuf[512];
		iResult = recv(*ClientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) 
		{
			handleMessages(*ClientSocket, recvbuf, iResult);
		}
		else if (iResult == 0)
		{

		}
		else 
		{
			printf("Server recv failed: %d\n", WSAGetLastError());
		}
	} while (iResult > 0);
	closesocket(*ClientSocket);
	onDisconnectClient(*ClientSocket);
}



TCPServer::TCPServer(const char* address, const char *port):
	m_address(address),
	m_port(port)
{
	m_treadListen = std::thread(&TCPServer::lisen,this);
}

TCPServer::~TCPServer()
{
	closesocket(ListenSocket);
	m_treadListen.join();
	while (m_sockets.size())
	{
		auto it = m_sockets.begin();
		closesocket(*(it->first));
		it->second.join();
		m_sockets.erase(it);
	}
}

bool TCPServer::write(SOCKET& ClientSocket, const char* data, int size)
{
	if (send(ClientSocket, data, size, 0) == SOCKET_ERROR) 
	{
		printf("Server send failed: %d\n", WSAGetLastError());
		return true;
	}
	return false;
}


void TCPServer::lisen()
{
	{
		struct addrinfo* result = NULL, * ptr = NULL, hints;
		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;
		int checkResult = getaddrinfo(m_address, m_port, &hints, &result);
		if (checkResult != 0) 
		{
			printf("Server getaddrinfo failed: %d\n", checkResult);
			return;
		}
		ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
		if (ListenSocket == INVALID_SOCKET) 
		{
			printf("Server Error at socket(): %ld\n", WSAGetLastError());
			freeaddrinfo(result);
			return;
		}
		checkResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
		if (checkResult == SOCKET_ERROR) 
		{
			printf("Server bind failed with error: %d\n", WSAGetLastError());
			freeaddrinfo(result);
			closesocket(ListenSocket);
			return;
		}
		freeaddrinfo(result);
	}
	while (listen(ListenSocket, SOMAXCONN) != SOCKET_ERROR) 
	{
		auto it = m_sockets.begin();
		while (it != m_sockets.end())
		{
			if (*(it->first) == INVALID_SOCKET) 
			{
				closesocket(*(it->first));
				it->second.join();
				m_sockets.erase(it);
				it = m_sockets.begin();
				continue;
			}
			it++;
		}

		std::shared_ptr<SOCKET> ClientSocket = std::make_shared<SOCKET>(accept(ListenSocket, NULL, NULL));

		if (*ClientSocket == INVALID_SOCKET) 
		{
			closesocket(ListenSocket);
			return;
		}

		m_sockets[ClientSocket] = std::thread(&TCPServer::handler, this, ClientSocket);
		onConnectClient(*ClientSocket);
	}
	printf("Server Listen failed with error: %ld\n", WSAGetLastError());
	closesocket(ListenSocket);
}

