#ifndef TCPCLIENT_H
#define TCPCLIENT_H
#include "Header.h"

class TCPClient
{
public:
	TCPClient(const char* address, const char* port);
	~TCPClient();
	int write(std::string m);
	void disconnect();
	bool reconnect();
private:
	void resive();
	SOCKET ConnectSocket = INVALID_SOCKET;
	std::thread m_treadResive;
	const char* m_port;
	const char* m_address;
};

#endif // TCPCLIENT_H

