#ifndef MQTTSERVER_H
#define MQTTSERVER_H

#include "TCPServer.h"

namespace MQTTBroker
{
	bool initLib();
	void deinitLib();

	enum MessType:char
	{
		UNKNOWN = 0, 
		CONNECT,	//������ ���������� � ��������
		CONNACK,		//������������� ����������
		PUBLISH,		//���������� ���������
		PUBACK,			//������������� ����������
		PUBREC,			//���������� �����������
		PUBREL,			//"�����" ����������
		PUBCOMP,		//���������� ����������
		SUNSCRIBE,		//������ ��������
		SUBACK,			//������������� ��������
		UNSUBSCRIBE,	//������ ����������
		UNSUBACK,		//������������� ������� ����������
		PINGREQ,		//PING ������
		PINGRESP,		//PiNG �����
		DISCONNECT,		//������������ �������
	};

	enum PackageStatus:char {
		EMPTY,
		TYPEREAD,
		LENREAD,
		DATAREAD,
		COMPLETED,
		NOTVALID
	};

	typedef struct Package
	{
		MessType type = MessType::UNKNOWN;
		int size = 0;
		PackageStatus status = PackageStatus::EMPTY;
		std::string data = "";
	};

	class MQTTServer :public TCPServer
	{
	public:
		MQTTServer(const char* address, const char* port);
		void handleMessages(SOCKET& ClientSocket, const char* data, int size) override;
		void onConnectClient(SOCKET& ClientSocket) override;
		void onDisconnectClient(SOCKET& ClientSocket) override;
	protected:
		virtual void onReadPackege(SOCKET& ClientSocket, Package& package);
	private:
		std::map<SOCKET, Package> wait;
	};
}

#endif //MQTTSERVER_H