#ifndef TCPSERVER_H
#define TCPSERVER_H
#include "Header.h"
#include <map>

class TCPServer
{
public:
	TCPServer(const char* address, const char *port);
	~TCPServer();
	bool write(SOCKET& ClientSocket, const char* data, int size);
	virtual void handleMessages(SOCKET& ClientSocket, const char* data, int size){}
	virtual void onConnectClient(SOCKET& ClientSocket){}
	virtual void onDisconnectClient(SOCKET& ClientSocket){}

private:
	void handler(std::shared_ptr<SOCKET> ClientSocket);
	void lisen();
	SOCKET ListenSocket = INVALID_SOCKET;
	std::map<std::shared_ptr<SOCKET>,std::thread> m_sockets;
	std::thread m_treadListen;
	const char* m_port;
	const char* m_address;
};

#endif // TCPSERVER_H